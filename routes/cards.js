var express = require('express');
var router = express.Router();

var Card = require('../models/card');

// returns all the cards
router.get('/', function(req, res, next) {
  Card.find(function(error, cards) {
    if (error) {
        res.status(500).json({
            error: "someCode",
            message: "Error with fetching the cards"
        });
    } else {
        res.status(200).json({
          error: null,
          message: "OK",
          data: cards
        });
    }
  });
});

// saves a new card
router.post('/', function(req, res) {
  if (req.body.answer.length > 0) {
    var card = new Card({
      question: req.body.question,
      answer: req.body.answer
    });
  
    card.save().then(function(l) {
      res.status(200).json({
        error: null,
        message: "success"
      })
    }).catch(function(err) {
      console.log(err);
    });
  } else {
    res.status(400).json({
      error: "Card does not have a valid answer",
      message: "The answer has to be at least 1 character"
    });
  }
});

// patches an existing card
router.patch('/:cardId', function(req, res, next) {
  if (req.body.answer.length > 0) {
    Card.findByIdAndUpdate(req.params.cardId, { $set: {question: req.body.question, answer: req.body.answer}}, {new: true}, function(error, card) {
      if (error) {
          res.status(500).json({
              error: "someCode",
              message: "Problem with fetching the card"
          })
      } else {
          // res.status(200).json({
          //     error: null,
          //     message: "OK",
          //     data: card
          // });
          Card.find(function(error, cards) {
            if (error) {
                res.status(500).json({
                    error: "someCode",
                    message: "Error with fetching the cards"
                });
            } else {
                res.status(200).json({
                  error: null,
                  message: "OK",
                  data: cards
                });
            }
          });
      }
    });
  } else {
    res.status(400).json({
      error: "Card does not have a valid answer",
      message: "The answer has to be at least 1 character"
    });
  }
  
});

module.exports = router;
