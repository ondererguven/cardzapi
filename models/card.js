var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// card schema to be used by mongoDB
var cardSchema = new Schema({
    question: String,
    answer: String
});

var Card = mongoose.model('Card', cardSchema);

module.exports = Card;